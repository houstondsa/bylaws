const { src, dest, series, watch } = require('gulp');
const markdown = require('gulp-markdown');
const concat = require('gulp-concat');
const wrap = require('gulp-wrap');
const sass = require('gulp-sass');
const minifyCSS = require('gulp-csso');
const html2pdf = require('gulp-html2pdf');
const path = require('path');
const moment = require('moment');


const template = "\
  <html>\
    <head>\
      <meta charset=\"utf-8\">\
      <link rel=\"stylesheet\" href=\"styles/styles.css\"/>\
    </head>\
    <body>\
      <header>\
        <h1>Bylaws of the Houston Democratic Socialists of America, Inc.</h1>\
        <time datetime=\"<%= timestamp.format() %>\">\
          Generated <%= timestamp.format('LL') %>\
        </time>\
      </header>\
      <%= contents %>\
    </body>\
  </html>\
"

const data = {
  timestamp: moment()
}

function html() {
  return src('src/*.md')
    .pipe(markdown())
    .pipe(concat('bylaws.html'))
    .pipe(wrap(template, data))
    .pipe(dest('dist'))
}

function css() {
  return src('src/styles/*.scss')
    .pipe(sass())
    .pipe(concat('styles.css'))
    .pipe(dest('dist/styles'))
}

function pdf() {
  return src('dist/bylaws.html')
    .pipe(html2pdf({ userStyleSheet: path.resolve('dist/styles/styles.css')}))
    .pipe(dest('dist'))
}

// exports.html = html;
// exports.css = css;
exports.watch = watch('src/**/*.*', series(html, css, pdf))
exports.build = series(html, css, pdf);