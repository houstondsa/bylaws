## Houston DSA Strategy

The Houston Local of the Democratic Socialists of America seeks to facilitate the transition to a truly democratic and socialist society, one in which the means/resources of production are democratically and socially controlled.

DSA rejects an economic order based on private profit, alienated labor, gross inequalities of wealth and power, discrimination based on race and sex, and brutality and violence in defense of the status quo.

DSA envisions a humane social order based on popular control of resources and production, economic planning, equitable distribution, gender and racial equality, and non-oppressive relationships.

Our conception of socialism is a profoundly democratic one. It is rooted in the belief that human beings should be free to develop to their fullest potential, and that public policies should be determined not by wealth but by popular participation, and that individual liberties should be carefully safeguarded. It is committed to a freedom of speech that does not recoil from dissent, to a freedom to organize independent trade unions, women’s groups, political parties, and other formations – recognizing these as essential bulwarks against the dangers of an intrusive state. It is committed to a freedom of religion that acknowledges the rights of those for whom spiritual concerns are central.

We are socialists because we are developing a concrete strategy for achieving this vision. In the present, we are building a visible socialist presence within the broad democratic left. In the long run, we hope to build a majority movement capable of making democratic socialism a reality in the United States. Our strategy acknowledges the class structure of the U.S. society. This class structure means that there is a basic conflict of interest between those sectors with enormous economic power and the vast majority of the population.

Houston DSA is committed to working on the above-mentioned issues and supporting the efforts of labor unions in our area. We encourage our members to actively support and participate in the work of their labor unions and advocacy groups on those issues. Houston DSA believes that justice for the people of the U.S. is directly linked to justice for people throughout the world. Houston DSA will strive to have a diverse membership and leadership.
