## Membership

### Membership
The Local membership shall be defined as those individuals whose dues to national DSA are paid in full and who reside and/or work in the Local's jurisdiction as defined in these Bylaws.

### Privileges and Responsibilities of Membership
The Local membership shall be solely responsible for the approval of policies and guidelines for the Local's operation; the election of the Local's officers and its delegates to national, regional, and state conventions; voting on matters related to national policy; and making recommendations to the National Political Committee of DSA.

### Active Membership
A Local member is considered an Active Member for the purposes of these Bylaws provided they meet at least one of the following criteria: 

- Attended two of the three most recent Regular Local Meetings;
- Participate consistently in Working Groups or Committees, vouched for a chair of said Working Group(s) or Committee(s);
- Vouched for by a member of the Steering Committee.

### Dues
The Local may establish a voluntary system of one-time or recurring donations for its members. No additional dues beyond those required for national DSA membership are required for membership in Houston DSA. 

### Removal of Members
If a full member is found to be in substantial disagreement with the principles or policies of national DSA or strategy outlined in Article II, or if they are found to be consistently engaging in undemocratic, disruptive behavior, or advocates violence, the Local membership may vote to expel them from Houston DSA.

In order for such a finding to be made, another DSA member shall formally present written charges to the Local Steering Committee, which shall set the date of a Local meeting for deliberations on the charges. The accused member shall receive a copy of the written charges and notice of the meeting a minimum of two weeks before that meeting takes place.  

A vote to expel a member shall be conducted via secret ballot and requires a three-fourths majority of Local membership present to pass. 

An expelled member may appeal to the National Political Committee of DSA. The Local Steering Committee may refer a locally expelled member to the National Political Committee of DSA for expulsion from the National organization.
