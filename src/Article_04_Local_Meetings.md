## Local Meetings

### Local Meetings
The Local shall hold a regular meeting of its membership at least once per month. Any change to meeting time, date, or location must be announced via social media and email to allow for adequate notice. Regular Local meetings may be open to non-members.

### Quorum
A quorum of 50% of the average attendance at the last six months’ Local meetings, rounded to the nearest whole number1, is required to transact business. The quorum shall be announced at each regular Local meeting, and shall remain that number until the next regular Local meeting.

### Parliamentary Procedure
The conduct of Local business shall be governed by standing rules where not otherwise specified in these Bylaws.

### Participation
Participation in Local business shall be limited to all Local members in good standing who have attended at least one Local meeting as a member. This meeting may include the meeting at which the member joined DSA.

### Emergency Meetings
The Local Steering Committee may call an Emergency Meeting of the Local on five days’ notice when an urgent and important matter requires deliberation.
