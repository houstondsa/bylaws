## Local Officers: Powers and Duties

The officers of the Local, referred to as the Steering Committee, shall serve as the governing body of the Local.


### Eligibility
All Local members shall be considered eligible to hold Local officership who meet the definition of Active Membership as defined in these Bylaws.


### Officers
The Local Steering Committee shall consist of two Co-Chairs, a Recording Secretary, a Membership Secretary, an Communications Secretary, an Internal Secretary, and a Treasurer.

#### Co-Chairs

The Co-Chairs are the executive officers of the Local. They shall preside over Local meetings, serve as its official public spokespersons, serve as the Local’s primary contacts with other DSA locals and the national DSA, and initiate such actions and policies as the Local's general welfare may demand.

#### Recording Secretary

The Recording Secretary shall be responsible for maintaining the Local's official records, including Local meeting and Steering Committee meeting minutes, Local resolutions, Bylaws, and official member lists, and transferring official records in good condition to their successor.

The Recording Secretary shall record minutes of Local and Steering Committee meetings, oversee the timely posting of minutes, resolutions, and amendments to the Local website, and ensure Bylaws-proscribed communications to membership.

#### Membership Secretary

The Membership Secretary shall be responsible for the overall health and growth of the Local membership. 

The Membership Secretary shall oversee initiatives concerning membership growth and renewal; ensuring National dues are paid and current; increasing member engagement and participation; effectively mobilizing membership for meetings, campaigns, actions, and other events; and maintaining regular internal communication with membership about Local activity.

#### Treasurer

The Treasurer shall be responsible for the funds and financial records of the Local. All funds collected by the Local shall be turned over to the Treasurer. 

The Treasurer shall prepare an annual Local budget, deliver periodic financial reports at Local meetings and as requested by the Steering Committee, and ensure the Local's regulatory compliance. The Treasurer shall also maintain documentation on how to do the above.

#### Communications Secretary

The Communications Secretary shall be responsible for the Local’s external communications, including management of the Local's website, social media, email, and advertising; producing graphics, video, audio, print materials, and merchandise; and overseeing press relations and media outreach. The Communications Secretary shall also be responsible for replying to the Local's correspondence and inquiries.

#### Internal Secretary

The Internal Secretary shall be responsible for the operations, logistics, and administrative tasks of the Steering Committee; coordinating Local, Working Group, and Committee activity and maintaining effective communication between these groups and the Steering Committee; establishing the processes and best practices necessary to provide for the above; and training membership on, and ensuring adherence to, these processes and best practices.


### Term of Office

The term of office for all positions shall be one year.


### Elections

One Co-Chair, the Treasurer, the Recording Secretary, the Membership Secretary, and the Communications Secretary shall be elected at each year's April Local meeting; the remaining Co-Chair and the Internal Secretary shall be elected at each year's October Local meeting. 

Elections shall be conducted in accordance with the procedures defined in these Bylaws.


### Vacancies

#### Definition

An Local officer position shall be considered vacant: a) upon the position's creation; b) upon an officer's written resignation delivered to the Steering Committee; c) upon a change in an officer's membership status; or d) upon a successful recall vote as defined in these Bylaws.

#### Filling Vacancies

Vacancies in the Steering Committee shall be announced to the membership immediately and shall be filled via special election as defined in these Bylaws. The new officer shall serve the remainder of the term.

#### Temporary Officers

In cases where necessary chapter business must continue, the Steering Committee may vote to appoint a temporary officeholder to fill a vacancy until the special election takes place.


### Additional Duties

The Local Steering Committee may assign any additional temporary duties to an officer of the Local, so long as such assignments do not conflict with the designation of responsibilities outlined in these Bylaws.
