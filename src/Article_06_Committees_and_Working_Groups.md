## Committees and Working Groups


### Committees

#### Definition

A Committee is defined as a group of Houston DSA members tasked with specific functions related to Local administration and internal organizing.

#### Creation and Dissolution

Ad Hoc committees formed for a specific task or objective may be created by Steering Committee or simple majority vote of chapter membership; these committees shall dissolve when their task or objective is complete.

Standing committees formed for the purpose of ongoing administrative or internal organizing work may be created by the Steering Committee. Standing committees may be dissolved by the Steering Committee. 

#### Leadership

A Committee must elect at least one chair from its membership. Committee chairs shall meet the requirements of active membership as defined in these Bylaws. Elections shall be held at least once per year on a schedule determined by the Committee. The Steering Committee may appoint one or more interim Committee chairs as necessary.

Committee chairs are responsible for organizing and overseeing Committee business; coordinating the group's activities and needs with the Steering Committee; and other responsibilities outlined in these Bylaws.

#### Special Committees

These Bylaws may define special committees, the definitions and rules of which may differ from those defined in this Article, as necessary.


### Working Groups

#### Definition

A Working Group shall be defined as a group of Houston DSA members tasked with specific functions related to campaign-building and external organizing.

#### Creation and Dissolution

Proposals to create a Working Group shall be made by written resolution, endorsed by at least five members, and submitted to the Steering Committee for approval via simple majority vote. If the Steering Committee rejects such a proposal, a resolution may be brought to the next Local meeting for simple majority vote of membership.

A motion to dissolve a Working Group may be made at any Local meeting, and shall require a two-thirds vote of the membership present to pass.

#### Leadership

A Working Group must elect at least one chair from its membership. Working Group chairs shall meet the requirements of active membership as defined in these Bylaws. Elections shall be held at least once per year on a schedule determined by the Working Group. The Steering Committee may appoint one or more interim Working Group chairs as necessary.

Working Group chairs are responsible for organizing regular Working Group activities; coordinating the group's activities and needs with the Steering Committee; and other responsibilities outlined in these Bylaws.
