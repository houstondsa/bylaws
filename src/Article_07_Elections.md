## Elections


### Covered Positions

The procedures in this Article shall cover all Local officers, members of the Electoral Committee, and delegates and alternates to National, Regional, and/or State Conventions.


### Nominations Committee

Nominations for positions to be elected shall be solicited and recorded, and election votes counted, by an ad-hoc Nominations Committee consisting of three members, none of whom are running for any of the positions being elected. Each Nominations Committee shall be dissolved upon completion of the election.


### Regular Elections

#### Nominations

At the regular Local Meeting preceding a regular election, a Nominations Committee shall be selected by the membership present and nominations for elected positions shall open.

Nominations may be made at any point until the commencement of the election at the following regular Local Meeting.

#### Election Process

Elections shall be conducted via secret ballot and votes shall be counted via instant runoff voting. Each member shall have one vote. No proxy votes or bullet votes are permitted.


### Special Elections

#### Nominations

Nominations for special elections shall be opened at the first Local meeting following the special election announcement. 

Nominations may be made at any point until the commencement of the election at the following regular Local Meeting.

#### Election Process

Elections shall be conducted via secret ballot and votes shall be counted via instant runoff voting. Each member shall have one vote. No proxy votes or bullet votes are permitted.


### Recall Elections

A motion to recall an elected position may be made at any Local meeting. If the motion is successful, a recall election shall be held at the next Local meeting. A three-quarters majority of the membership present is required to recall an elected position.