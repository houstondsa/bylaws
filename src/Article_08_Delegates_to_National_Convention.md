## Delegates to the National Convention

Local delegates and alternates to National, Regional and State Conventions shall be elected in accordance with the procedures defined in these Bylaws.  

Elections for Convention delegations shall be held on the schedule announced by the national organization.