## Prohibited Activity

Houston Local shall not engage in activity prohibited by the IRS guidelines established for 501(c)(4) organizations or similar rules established by the state of Texas.  Nor shall the local engage in any activity prohibited by resolutions adopted by DSA’s National Convention or DSA’s National Political Committee.
