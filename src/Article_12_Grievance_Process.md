## Harassment Grievance Process

Houston DSA is committed to creating a space that is welcoming and inclusive to members of all genders, races, and classes. The following policy provides guidelines to ensure that everyone is able to organize without fear of harassment, abuse, or harm.



### Prohibited Behavior

Members of Houston DSA shall not engage in harassment on the basis of sex, gender, gender identity or expression, sexual orientation, physical appearance, disability, race, color, religion, national origin, immigration status, class, or age. Harassing or abusive behavior, such as unwelcome attention, inappropriate or offensive remarks, slurs, or jokes, physical or verbal intimidation, stalking, inappropriate physical contact or proximity, and other verbal and physical conduct constitute harassment when:

1. Submission to such conduct is made either explicitly or implicitly a term or condition of a member’s continued affiliation with DSA;
1. Submission or rejection of such conduct by an individual is used as the basis for organizational decisions affecting such individual; or
1. Such conduct has the purpose or effect of creating a hostile environment interfering with an individual’s capacity to organize within DSA.


#### Other Protected Classes.

Harassment based on categories not encompassed by those listed above shall be evaluated at the discretion of the Grievance Mediators and Steering Committee.



### Grievance Mediators

The Local shall select two of its members to serve as _Grievance Mediators_, who shall share the following responsibilities:

1.  Implementing the Grievance Process outlined in including all tasks and responsibilities outlined therein;
1.  Providing forms both for reporting and responding to accusations of harassment that include:
    1. The parties’ contact information
    1. The names of the parties involved
    1. A description of reported incident
1. Maintaining an archive of all accuser reports, written responses, and relevant correspondence;
1. Compiling an annual report that details:
	  1. How many reports were made;
	  1. How many cases were taken to the disciplinary process;
	  1. How many disciplinary actions were taken, and the nature of those actions;
	  1. Any recommended changes for making the reporting system more effective.

This report shall not include personally identifying information of any parties in any dispute. The report shall be delivered to the Local Steering Committee and the DSA National Harassment Grievance Officers no later than January 1 of the new year.


#### Eligibility

Any Houston DSA member is eligible to serve as a Grievance Mediator who a) does not currently serve as an elected Officer of the Local; or b) has not been previously recalled from a Grievance Mediator position as defined in Section 2.3 of these bylaws.


#### Appointment & Term of Office

Grievance Mediators shall be appointed by the Local Steering Committee. Appointments shall take effect immediately upon acceptance by the appointed member. The term for Grievance Mediators shall be one year, beginning and ending in October of each year.


#### Recall Process

The Steering Committee may not terminate the appointment of a Grievance Mediator. A motion to recall a Grievance Mediator may be made at any Local meeting; if successful, a recall vote shall be held at the subsequent Local meeting and must receive a three-fourths vote of the membership present to pass.


#### Vacancies

##### Definition
A Grievance Mediator position shall be considered vacant:

1.  Upon the Grievance Mediator's written resignation;
1.  Upon a change in a Grievance Mediator's National DSA membership status;
1.  Upon the Grievance Mediator's removal from membership; or
1.  Upon a successful recall vote as defined in Section 2.3.

##### Replacement
The Steering Committee shall appoint a new Grievance Mediator to fill any vacancy. The new Grievance Mediator shall serve the remainder of the term.



### Grievance Process

Houston DSA Members who believe they have been harassed by another member may follow the complaint process as defined in this section. There shall be no limit on the time period in which an accuser may file a report after alleged harassment has occurred.

#### Recusal

Grievance Mediators or Steering Committee members shall recuse themselves from any grievance process in which they are the accuser or the accused.


#### Filing a Complaint

Members who wish to file a formal complaint must contact one or both Houston DSA Grievance Mediators (GMs). The Local shall maintain an email address that is only accessible by the Grievance Mediators to function as a confidential reporting “hotline” for this purpose.

#### Reporting Procedure and Timeline

After a written report has been submitted, whether through the email hotline or otherwise:

1.  The GM(s) responsible for the reporting channel used by the accuser will contact the accused member within seven days to notify them that a report has been filed against them and request a written response to the report either affirming or denying its substance;
2.  The accused will submit their written response within seven days of being notified. If the accused does not meet this deadline, the GM(s) will recommend the Steering Committee move to take appropriate disciplinary action;
3.  If the accused denies the substance of the report, the GM(s) overseeing the dispute will have the option to investigate the report by:
    1.  interviewing other members with direct knowledge of the substance of the report;
    1.  requesting documentation from either the accuser or accused or any other parties directly involved; or
    1.  employing any and all other means deemed necessary, with the utmost respect for the confidentiality of the parties, within a time period not to exceed ten days.
1.  If necessary, GM(s) may recommend that parties do not contact each other for the duration of the investigative process.
1.  The GM(s) responsible for adjudicating the dispute will determine whether the report is credible and, if necessary, make a recommendation to Steering Committee of appropriate disciplinary action as soon as practicable, but ultimately within thirty days of the report being filed. This is to ensure the timely, efficient, accurate, and discreet adjudication of all reports. The GM(s) may notify Executive of the accuser’s report and its substance at any time after the report is filed, but must give written notice to both the accuser and the accused member before doing so.


#### Remedies and Penalties

##### Determinations

All reports shall be assessed on a case-by-case basis by the GMs and Local Steering Committee. The ultimate disposition of each report shall be made by the Steering Committee after that body reviews the written report and recommendation of the GM(s).

##### Standard for Determining Credibility of Reports

The Local Steering Committee shall find the factual allegation in a report is “credible” if it more-likely-than-not occurred.

##### Remedies and Penalties

If the Steering Committee finds the report to be credible, they are authorized to carry out the following remedies and penalties:

1. A formal discussion between the accused and the Steering Committee to develop a plan to change the harassing behavior(s);
1. Suspension from committee meetings and other Local or organizational events;
1. Removal from Local committees or working groups;
1. Removal from the Local in accordance with Article III of these Bylaws; and
1. Any and all other relief deemed necessary and just by the Local Steering Committee.

The appropriate form of relief will be determined by, among other things:

1. The request of the accuser;
1. The severity of the offense;
1. The response of the accused; and
1. The accused’s relevant behavioral histories.


#### Appeals

Either party may appeal the form of relief determined by Steering Committee to the National DSA Harassment Grievance Officer(s). Appeals must be filed within thirty days of receiving written notice of the Steering Committee’s decision.

The limited grounds for appeal are:

1. Either party believes the behavior was not interpreted using the standards for harassment set out in Section 1;
1. Procedural errors, misconduct, or conflicts of interest affected the fairness of the outcome; and
1. The remedy or penalty determined by the Steering Committee was grossly disproportionate to the violation committed


#### Retaliation

This policy prohibits retaliation against any member for bringing a complaint of harassment pursuant to this policy. This policy also prohibits retaliation against a person who assists someone with a complaint of harassment, or participates in any manner in an investigation or resolution of a complaint of discrimination or harassment. Retaliatory behaviors include threats, intimidation, reprisals, and/or adverse actions related to organizing. If any party to the complaint believes there has been retaliation, they may inform the Grievance Mediator who will determine whether to factor the retaliation into the original complaint or to treat it as a separate incident.
