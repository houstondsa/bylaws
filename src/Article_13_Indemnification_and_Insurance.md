## Indemnification and Insurance.

The organization shall indemnify and hold harmless any officer of the organization to the maximum extent allowed by [Charitable Liability and Immunity Act, Tex. Civ. Prac. & Rem Code Sec 84]. In providing indemnification under this Article XII, the organization shall follow the procedures described in Charitable Liability and Immunity Act, Tex. Civ. Prac. & Rem Code Sec 84]. Further, the organization shall indemnify and advance expenses to an officer who is party to a proceeding because they are or was an officer of the organization, except for:

1.  Liability in connection with a proceeding by or in the right of the organization other than for reasonable expenses incurred in connection with the proceeding; or
2.  Liability arising out of conduct that constitutes:
    1. Receipt by the officer of a financial benefit to which the officer is not entitled;
    2. an intentional infliction of harm on the organization or the members; or
    3. an intentional violation of criminal law.