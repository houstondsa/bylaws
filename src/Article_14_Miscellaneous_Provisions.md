## Misellaneous Provisions.

1.  Fiscal Year. The annual accounting period of the organization shall begin on January 1 of each year, unless changed by the Steering Committee.
2.  Checks. All checks, drafts, or other orders for the payment of money shall be signed by such Officer or Officers or such other person or persons as the Steering Committee may from time to time designate.
3.  Contracts. All contracts, notes or other evidences of indebtedness, and leases of space for the organization as ratified by the local membership at a monthly Local meeting shall be signed by such officer or officers or such other person or persons as the Steering Committee may from time to time designate.
4.  Records. The organization shall keep as permanent records minutes of all meetings of its Steering Committee, and any designated body, a record of all actions taken by the Steering Committee, or members of a designated body without a meeting, and a record of all actions taken by a committee of the Steering Committee or a designated body on behalf of the organization. A organization shall keep a copy of the following records: 
    1.  Articles of Incorporation or restated Articles of Incorporation and all amendments to them currently in effect; 
    2.  These Bylaws or restated bylaws and all amendments to them currently in effect;
    3.  Minutes and records described in this section for the past 3 years;
    4.  A list of the names and business addresses of its current directors and officers; and 
    5. The most recent quadrennial report filed with the State of Texas.