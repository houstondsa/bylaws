## Endorsements

### Endorsements Committee
The Endorsements Committee shall be tasked with vetting and screening electoral candidates for endorsement and providing recommendations for endorsement or support to the Local Membership.

#### Composition
The Endorsements Committee shall consist of nine voting members, two of which are reserved for the Local Co-Chairs _ex officio_. 

#### Elections & Term
The remaining seven members shall be elected by the Local membership. Elections shall take place at each year’s December Local meeting and shall be conducted in accordance with the procedures defined in these Bylaws. Elected committee members' term of office shall be one year.

#### Leadership
The Committee shall elect a chair, who is charged with coordinating the committee’s activities and serving as point person to candidates.

#### Vacancies
An Electoral Committee position shall be considered vacant a) upon a committee member's written resignation delivered to the Steering Committee; b) upon a change in a committee member's membership status; or c) upon a successful recall vote as created in these Bylaws. 

Vacancies in the Electoral Committee shall be announced to the membership immediately and shall be filled via special election as defined in these Bylaws. The new committee member shall serve the remainder of the term.


### Endorsement Process

#### Research

Volunteers from the committee will research candidates’ backgrounds, including but not limited to voting history, previous votes or decision-making, donation history, news or press appearances, memberships, and associations.

#### Questionnaire

The committee will provide the candidate with a questionnaire relevant to the office being sought, and will set a submission deadline.

#### Screening

The Electoral Committee will meet with the candidate to discuss the candidate’s answers to the questionnaire, along with any other issues the committee would like the candidate to address. These meetings are open to the Local membership.

#### Recommendation

The Electoral Committee shall vote on a recommendation to the Local membership on whether or not to endorse or offer support to the candidate. Electoral Committee recommendations shall pass by simple majority.

#### Vote

The Committee shall bring all candidate endorsements before the Local membership for a debate and vote. The committee will present its recommendations, including both majority and minority reports, before voting. Endorsement requires a two-thirds majority of the membership present.